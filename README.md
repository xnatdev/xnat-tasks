# XNAT Tasks Library #

This is the XNAT tasks library, used by [XDAT Data Builder](https://bitbucket.org/xnatdev/xdat-data-builder), [XNAT Data Builder](https://bitbucket.org/xnatdev/xnat-data-builder), and other tools to generate code, SQL, and the like. Source code for this library can be found on the (xnat-tasks source repository](https://bitbucket.org/xnatdev/xnat-tasks).
