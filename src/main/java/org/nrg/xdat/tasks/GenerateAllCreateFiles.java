/*
 * GenerateAllCreateFiles
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:12 AM
 */

package org.nrg.xdat.tasks;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.commandPrompt.CommandPromptTool;
import org.nrg.xft.generators.JavaFileGenerator;
import org.nrg.xft.generators.JavaScriptGenerator;

import java.io.File;
import java.util.Hashtable;

public class GenerateAllCreateFiles extends CommandPromptTool {
    public GenerateAllCreateFiles(String[] args) {
        super(args);
    }

    public static void main(String[] args) {
        new GenerateAllCreateFiles(args);
    }

    public boolean requireLogin() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.nrg.xft.commandPrompt.CommandPromptTool#definePossibleVariables()
     */
    public void definePossibleVariables() {
        addPossibleVariable("javadir", "Root directory of Java Source.", false);
        addPossibleVariable("sqlfile", "SQL Output.", false);
        addPossibleVariable("templateDir", "Root directory of Templates.", false);
        addPossibleVariable("srcControlDir", "Directory to check for existing Java source files.", false);
        addPossibleVariable("javascriptDir", "Root directory of Java script Source.", false);
        addPossibleVariable("displayDocs", "if (true), XDAT will generate default display.xml files for each root level data type.", false);
    }

    /* (non-Javadoc)
     * @see org.nrg.xft.commandPrompt.CommandPromptTool#getAdditionalUsageInfo()
     */
    public String getAdditionalUsageInfo() {
        return "";
    }

    /* (non-Javadoc)
     * @see org.nrg.xft.commandPrompt.CommandPromptTool#getDescription()
     */
    public String getDescription() {
        return "Function used to generate files objects which allow for easy access and customization of data.\n";
    }

    /* (non-Javadoc)
     * @see org.nrg.xft.commandPrompt.CommandPromptTool#getName()
     */
    public String getName() {
        return "GenerateAllCreateFiles";
    }

    public void process() {
        Hashtable hash = variables;
        try {
            String javaDir = StringUtils.defaultIfBlank((String) hash.get("javadir"), directory);

            if (!javaDir.endsWith(File.separator)) {
                javaDir += File.separator;
            }

            String srcControlDir = StringUtils.defaultIfBlank((String) hash.get("srcControlDir"), directory);

            if (!srcControlDir.endsWith(File.separator)) {
                srcControlDir += File.separator;
            }

            String sqlfile = StringUtils.defaultIfBlank((String) hash.get("sqlfile"), directory);

            if (!sqlfile.endsWith(File.separator))
                sqlfile += File.separator;

            String javascriptdir = StringUtils.defaultIfBlank((String) hash.get("javascriptdir"), directory);

            if (!javascriptdir.endsWith(File.separator))
                javascriptdir += File.separator;

            boolean generateDisplayDocs = BooleanUtils.toBoolean((String) hash.get("displayDocs"));

            String templateDir = StringUtils.defaultIfBlank((String) hash.get("templateDir"), directory);

            if (!templateDir.endsWith(File.separator))
                templateDir += File.separator;

            if (XFT.VERBOSE)
                System.out.println("Generating files...");

            JavaFileGenerator.GenerateJavaFiles(javaDir, templateDir, true, generateDisplayDocs, srcControlDir);
            JavaScriptGenerator.GenerateJSFiles(javascriptdir, false);
            tool.generateSQL(sqlfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
